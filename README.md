# go bindings to libevdev #

c binding wrapper to the linux input event stack. allows creation of virtual input devices, key logging of real devices etc.
may support macosx in the future (unlikely as I don't own a mac but I hope to someday so small work on that.)

easy and straight forward to use. see examples directory.