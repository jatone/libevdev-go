package libevdev

import "fmt"

func PrintEvent(etype, code, value int) {
	fmt.Println(EventCodeGetName(etype, code), value)
}
