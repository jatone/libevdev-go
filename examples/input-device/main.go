package main

import (
	"log"
	"time"

	"bitbucket.org/jatone/libevdev/devices"
)

// Simple example - creates a keyboard device and emits random key strokes into
// the device.
func main() {
	var err error

	device, err := devices.Compose(devices.NewNamedDevice("My Keyboard"),
		devices.Synchronization,
		devices.DefaultKeyboard,
	)
	stop(err)

	log.Println("Getting user input handle from device", device.GetName())
	uidev, err := device.ManagedUserInputDevice()
	stop(err)

	t1 := time.Tick(1 * time.Second)
	for {
		select {
		case <-t1:
			if err := devices.RandomKeyStroke(uidev); err != nil {
				stop(err)
			}
		}
	}
}

func stop(message ...interface{}) {
	e := message[len(message)-1]
	if e != nil {
		log.Fatalln(message...)
	}
}
