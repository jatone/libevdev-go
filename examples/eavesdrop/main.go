package main

import (
	"log"
	"os"
	"syscall"

	"bitbucket.org/jatone/libevdev"
)

type event struct {
	_type int
	code  int
	value int
}

// Simple example - creates a keyboard device and emits random key strokes into
// the device.
func main() {
	events := make(chan event, 100)
	for _, path := range os.Args[1:] {
		dev, err := libevdev.OpenDevice(path, syscall.O_RDONLY|libevdev.BlockingDevice)
		stop(err)
		log.Println("opened:")
		log.Println("name", dev.GetName())
		log.Println("vendor", dev.GetVendor())
		log.Println("product", dev.GetProduct())
		log.Println("version", dev.GetVersion())
		log.Println("location", dev.GetPhysicalLocation())
		go readdevice(dev, events)
	}

	for event := range events {
		libevdev.PrintEvent(event._type, event.code, event.value)
	}
}

func readdevice(d libevdev.Device, events chan event) {
	for {
		etype, code, value, err := d.Eavesdrop(libevdev.ReadFlagNormal | libevdev.ReadFlagBlocking)
		if err != nil {
			log.Println("error reading event", err)
		} else {
			events <- event{_type: etype, code: code, value: value}
		}
	}
}

func stop(message ...interface{}) {
	e := message[len(message)-1]
	if e != nil {
		log.Fatalln(message...)
	}
}
