package libevdev

import "syscall"

// NewDevice returns an new ev Device.
func NewDevice() (dev Device) {
	dev = newDevice(libevdevNew())
	return
}

func newDevice(d *libevDevice) (dev Device) {
	dev = Device{libevDevice: d}
	return
}

// OpenDevice returns the ev Device specified by the given file path.
// OpenDevice("/dev/input/event0", syscall.O_RDONLY) - see syscall.Open
func OpenDevice(path string, mode int) (Device, error) {
	var dev *libevDevice
	var err error
	var fd int

	if fd, err = syscall.Open(path, mode, 0660); err != nil {
		return Device{dev}, err
	}

	dev, err = libevdevNewFromFd(fd)

	return Device{dev}, err
}

// Device holds underlying pointer to the ev device.
type Device struct {
	*libevDevice
}

// UserInputDevice create a user input device for this device.
// fd can be created using syscall.Open
func (t Device) UserInputDevice(fd int) (UserInputDevice, error) {
	var uidev userInputDevice
	var err error

	uidev.libevUserInputDevice, err = libevdevUInputCreateFromDevice(t.libevDevice, fd)

	return uidev, err
}

// ManagedUserInputDevice Convience method that creates a managed user input device.
func (t Device) ManagedUserInputDevice() (UserInputDevice, error) {
	return t.UserInputDevice(ManagedInputDevice)
}

// SetName sets the name of the device.
func (t Device) SetName(name string) {
	libevdevSetName(t.libevDevice, name)
}

// SetVendor sets the vendor id of the device.
func (t Device) SetVendor(id int) {
	libevdevSetVendor(t.libevDevice, id)
}

// SetProduct sets the product id of the device.
func (t Device) SetProduct(id int) {
	libevdevSetProduct(t.libevDevice, id)
}

// SetVersion sets the version id of the device.
func (t Device) SetVersion(id int) {
	libevdevSetVersion(t.libevDevice, id)
}

// GetName returns the name of the device.
func (t Device) GetName() string {
	return libevdevGetName(t.libevDevice)
}

// GetVendor returns the vendor id of the device
func (t Device) GetVendor() int {
	return libevdevGetVendor(t.libevDevice)
}

// GetProduct returns the product id of the device
func (t Device) GetProduct() int {
	return libevdevGetProduct(t.libevDevice)
}

// GetVersion returns the version id of the device
func (t Device) GetVersion() int {
	return libevdevGetVersion(t.libevDevice)
}

// GetPhysicalLocation returns the physical location (if any) of the device.
func (t Device) GetPhysicalLocation() string {
	return libevdevGetPhys(t.libevDevice)
}

// GetFD return the file descriptor for the device
func (t Device) GetFD() int {
	return libevdevGetFd(t.libevDevice)
}

// EnableEventType enabled the provided event for the device.
func (t Device) EnableEventType(eventType int) error {
	return libevdevEnableEventType(t.libevDevice, eventType)
}

// DisableEventType disables the provided event type for the device.
func (t Device) DisableEventType(eventType int) error {
	return libevdevDisableEventType(t.libevDevice, eventType)
}

// EnableEventCode enables the specified event for the given type.
// does not yet support types EV_ABS, EV_REP.
func (t Device) EnableEventCode(eventType, code int) error {
	return libevdevEnableEventCode(t.libevDevice, eventType, code)
}

// EnableEventCodes convience method for enabling many event codes at once.
// stops at the first key that returns an error
func (t Device) EnableEventCodes(eventType int, codes ...int) error {
	for _, code := range codes {
		if err := t.EnableEventCode(eventType, code); err != nil {
			return err
		}
	}
	return nil
}

// DisableEventCode disabled the specified event for the given type.
func (t Device) DisableEventCode(eventType, code int) error {
	return libevdevDisableEventCode(t.libevDevice, eventType, code)
}

func (t Device) Eavesdrop(flags uint) (eventType, code, value int, err error) {
	return libevdevNextEvent(t.libevDevice, flags)
}
