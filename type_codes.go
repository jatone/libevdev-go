package libevdev

/*
#include <linux/input.h>
*/
import "C"

type eventTypes struct {
	SYN, KEY, REL, ABS, MSC, LED, SND, REP, FF, PWR, FF_STATUS, MAX int
}

var Type = eventTypes{
	SYN:       int(C.EV_SYN),
	KEY:       int(C.EV_KEY),
	REL:       int(C.EV_REL),
	ABS:       int(C.EV_ABS),
	MSC:       int(C.EV_MSC),
	LED:       int(C.EV_LED),
	SND:       int(C.EV_SND),
	REP:       int(C.EV_REP),
	FF:        int(C.EV_FF),
	PWR:       int(C.EV_PWR),
	FF_STATUS: int(C.EV_FF_STATUS),
	MAX:       int(C.EV_MAX),
}

const KEY_MAX int = int(C.KEY_MAX)
