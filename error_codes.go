package libevdev

/*
#include <string.h>
#include <stdlib.h>
*/
import "C"

const generalError = -1
const success = 0

func strerror(code C.int) string {
	cstring := C.strerror(code)
	return C.GoString(cstring)
}

// libevdev likes to convert error codes to negative values.
func libevdev_strerror(code C.int) string {
	return strerror(code * -1)
}
