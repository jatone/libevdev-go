package libevdev

// relative axes
const (
	REL_X      int = 0x00
	REL_Y      int = 0x01
	REL_Z      int = 0x02
	REL_RX     int = 0x03
	REL_RY     int = 0x04
	REL_RZ     int = 0x05
	REL_HWHEEL int = 0x06
	REL_DIAL   int = 0x07
	REL_WHEEL  int = 0x08
	REL_MISC   int = 0x09
	REL_MAX    int = 0x0f
)
