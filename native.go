// +build linux

package libevdev

/*
#cgo pkg-config: libevdev
#include <stdlib.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>
#include <sys/sysmacros.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/input.h>
*/
import "C"
import (
	"fmt"
	"log"
	"runtime"
	"syscall"
)

// ManagedInputDevice ....
const ManagedInputDevice = C.LIBEVDEV_UINPUT_OPEN_MANAGED

// Put the device into blocking mode.
const BlockingDevice = 0 &^ syscall.O_NONBLOCK

const ReadFlagNormal = C.LIBEVDEV_READ_FLAG_NORMAL
const ReadFlagBlocking = C.LIBEVDEV_READ_FLAG_BLOCKING

// local type definition for libevdev device struct.
type libevDevice struct {
	ptr *C.struct_libevdev
}

// releases the memory returned by the c code.
func libevDeviceFinalize(dev *libevDevice) {
	// nothing to do if ptr is nil.
	if dev.ptr == nil {
		return
	}

	// -1 indicates the fd was never set for the provided device.
	if fd := int(C.libevdev_get_fd(dev.ptr)); fd > -1 {
		if err := syscall.Close(fd); err != nil {
			log.Println("Error closing file descriptor", err)
		}
	}

	C.libevdev_free(dev.ptr)
}

// releases the memory returned by the c code.
func libevInputDeviceFinalize(dev *libevUserInputDevice) {
	// nothing to do if ptr is nil.
	if dev.ptr == nil {
		return
	}

	// -1 indicates the fd was never set for the provided device.
	if fd := int(C.libevdev_uinput_get_fd(dev.ptr)); fd > -1 {
		if err := syscall.Close(fd); err != nil {
			log.Println("Error closing file descriptor", err)
		}
	}

	C.libevdev_uinput_destroy(dev.ptr)
}

// local type definition for the libevdev user input device struct.
type libevUserInputDevice struct {
	ptr *C.struct_libevdev_uinput
}

func newlibevUserInputDevice() *libevUserInputDevice {
	dev := &libevUserInputDevice{}
	runtime.SetFinalizer(dev, libevInputDeviceFinalize)

	return dev
}

func newlibevDevice() *libevDevice {
	dev := &libevDevice{}
	runtime.SetFinalizer(dev, libevDeviceFinalize)

	return dev
}

func libevdevNew() (dev *libevDevice) {
	dev = newlibevDevice()
	dev.ptr = C.libevdev_new()
	return
}

func libevdevNewFromFd(fd int) (dev *libevDevice, err error) {
	dev = newlibevDevice()

	if errcode := C.libevdev_new_from_fd(C.int(fd), &dev.ptr); errcode != 0 {
		// make sure the memory we allocated gets cleaned up properly
		C.libevdev_free(dev.ptr)
		// remove the reference from the device structure
		dev.ptr = nil
		// nil out the return value.
		dev = nil
		err = fmt.Errorf("failed initializing Device: %s", libevdev_strerror(errcode))
	}

	return
}

// libevdevUInputCreateFromDevice - create user input for a device.
func libevdevUInputCreateFromDevice(dev *libevDevice, fd int) (idev *libevUserInputDevice, err error) {
	idev = &libevUserInputDevice{}
	if errcode := C.libevdev_uinput_create_from_device(dev.ptr, C.int(fd), &(idev.ptr)); errcode != 0 {
		// make sure the memory we allocated gets cleaned up properly
		C.libevdev_uinput_destroy(idev.ptr)
		// remove the reference from the device structure
		idev.ptr = nil
		// nil out the return value.
		idev = nil

		err = fmt.Errorf("failed initializing Device: %s", libevdev_strerror(errcode))
	}

	return
}

// libevdevEnableEventType enabled the provided event for the device.
func libevdevEnableEventType(dev *libevDevice, eventType int) error {
	switch C.libevdev_enable_event_type(dev.ptr, C.uint(eventType)) {
	case success:
		return nil
	default:
		return fmt.Errorf("failed enabling event type %d", eventType)
	}
}

// libevdevDisableEventType disables the provided event type for the device.
func libevdevDisableEventType(dev *libevDevice, eventType int) error {
	switch C.libevdev_disable_event_type(dev.ptr, C.uint(eventType)) {
	case success:
		return nil
	default:
		return fmt.Errorf("failed disabling event type %d", eventType)
	}
}

func libevdevEnableEventCode(dev *libevDevice, eventType, code int) error {
	switch C.libevdev_enable_event_code(dev.ptr, C.uint(eventType), C.uint(code), nil) {
	case success:
		return nil
	default:
		return fmt.Errorf("failed enabling event code %d for type %d", code, eventType)
	}
}

func libevdevDisableEventCode(dev *libevDevice, eventType, code int) error {
	switch C.libevdev_disable_event_code(dev.ptr, C.uint(eventType), C.uint(code)) {
	case success:
		return nil
	default:
		return fmt.Errorf("failed disabling event code %d for type %d", code, eventType)
	}
}

// libevdevSetName sets the name of the device.
func libevdevSetName(dev *libevDevice, name string) {
	// TODO check if this leaks memory, pretty sure libevdev takes control of the ptr
	// and does not copy it.
	C.libevdev_set_name(dev.ptr, C.CString(name))
}

func libevdevSetProduct(dev *libevDevice, id int) {
	C.libevdev_set_id_product(dev.ptr, C.int(id))
}

func libevdevSetVendor(dev *libevDevice, id int) {
	C.libevdev_set_id_vendor(dev.ptr, C.int(id))
}

func libevdevSetVersion(dev *libevDevice, id int) {
	C.libevdev_set_id_version(dev.ptr, C.int(id))
}

// libevdevGetName returns the name of the given device.
func libevdevGetName(dev *libevDevice) string {
	// TODO check if this leaks memory
	return C.GoString(C.libevdev_get_name(dev.ptr))
}

func libevdevGetProduct(dev *libevDevice) int {
	return int(C.libevdev_get_id_product(dev.ptr))
}

func libevdevGetVendor(dev *libevDevice) int {
	return int(C.libevdev_get_id_vendor(dev.ptr))
}

func libevdevGetVersion(dev *libevDevice) int {
	return int(C.libevdev_get_id_version(dev.ptr))
}

func libevdevGetFd(dev *libevDevice) int {
	return int(C.libevdev_get_fd(dev.ptr))
}

// libevdevEmit emits a user input event.
func libevdevEmit(dev *libevUserInputDevice, eventType, code, value int) error {
	ceventType, ccode, cvalue := C.uint(eventType), C.uint(code), C.int(value)
	if errcode := C.libevdev_uinput_write_event(dev.ptr, ceventType, ccode, cvalue); errcode != 0 {
		return fmt.Errorf("failed emitting event: type: %d, code: %d, error: %s", eventType, code, libevdev_strerror(errcode))
	}
	return nil
}

func libevdevNextEvent(dev *libevDevice, flags uint) (eventType, code, value int, err error) {
	var e C.struct_input_event
	if errcode := C.libevdev_next_event(dev.ptr, C.uint(flags), &e); errcode != 0 {
		return 0, 0, 0, fmt.Errorf(libevdev_strerror(errcode))
	}
	return int(e._type), int(e.code), int(e.value), nil
}

func libevdevGetPhys(dev *libevDevice) string {
	cstring := C.libevdev_get_phys(dev.ptr)
	return C.GoString(cstring)
}

// EventCodeGetName returns human friendly name for the given type and code.
func EventCodeGetName(eventType, code int) string {
	cstring := C.libevdev_event_code_get_name(C.uint(eventType), C.uint(code))
	return C.GoString(cstring)
}

// EventTypeGetName returns human friendly name for the given type.
func EventTypeGetName(eventType int) string {
	cstring := C.libevdev_event_type_get_name(C.uint(eventType))
	return C.GoString(cstring)
}
