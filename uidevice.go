package libevdev

type UserInputDevice interface {
	Emit(eventType, code, value int) error
}

type userInputDevice struct {
	*libevUserInputDevice
}

// Emit an event
func (t userInputDevice) Emit(eventType, code, value int) error {
	return libevdevEmit(t.libevUserInputDevice, eventType, code, value)
}
