package devices

import "bitbucket.org/jatone/libevdev"

type composer func(dev libevdev.Device) (device libevdev.Device, err error)

// Compose allows for enabling many event types and codes using predefined
// functions.
func Compose(dev libevdev.Device, composers ...composer) (device libevdev.Device, err error) {
	device = dev
	for _, compose := range composers {
		if device, err = compose(device); err != nil {
			return device, err
		}
	}

	return device, err
}

// Synchronization configures the device to enable SYN SYN_REPORT events.
func Synchronization(device libevdev.Device) (libevdev.Device, error) {
	return device, device.EnableEventCode(libevdev.Type.SYN, libevdev.SYN_REPORT)
}
