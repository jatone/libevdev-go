package devices

import "bitbucket.org/jatone/libevdev"

// DefaultMedia configures the provided device to enable media control event codes.
func DefaultMedia(device libevdev.Device) (libevdev.Device, error) {
	err := checkForErrors(
		device.EnableEventCodes(libevdev.Type.KEY, soundControl...),
		device.EnableEventCodes(libevdev.Type.KEY, mediaControl...),
		device.EnableEventCodes(libevdev.Type.KEY, libevdev.KEY_STOPCD),
	)
	return device, err
}
