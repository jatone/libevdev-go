package devices

import (
	"math/rand"

	"bitbucket.org/jatone/libevdev"
)

// RandomCode selects a random key from the provided set of keys.
// meant as a convience function for example code, test programs.
func RandomCode(codes ...int) int {
	return codes[rand.Intn(len(codes))]
}

// RandomKeyStroke generates a random key press from the keyboard key set.
func RandomKeyStroke(emitter libevdev.UserInputDevice) error {
	code := RandomCode(KeySets.Keyboard...)
	if err := emitter.Emit(libevdev.Type.KEY, code, 1); err != nil {
		return err
	}

	if err := emitter.Emit(libevdev.Type.KEY, code, 0); err != nil {
		return err
	}

	return emitter.Emit(libevdev.Type.SYN, libevdev.SYN_REPORT, 0)
}

// RandomMouseMovement generates a random mouse movement with relative positions within
// the -5 to 5 range.
func RandomMouseMovement(emitter libevdev.UserInputDevice) error {
	code := RandomCode(libevdev.REL_X, libevdev.REL_Y)
	val := (rand.Intn(10) + 1) - 5
	return emitter.Emit(libevdev.Type.REL, code, val)
}
