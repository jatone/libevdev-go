package devices

import "bitbucket.org/jatone/libevdev"

// DefaultMouse configures the provided device to enable common mouse event codes.
func DefaultMouse(device libevdev.Device) (libevdev.Device, error) {
	err := checkForErrors(
		device.EnableEventCodes(libevdev.Type.REL, libevdev.REL_X, libevdev.REL_Y),
		device.EnableEventCodes(libevdev.Type.REL, libevdev.REL_Z, libevdev.REL_RX),
		device.EnableEventCodes(libevdev.Type.REL, libevdev.REL_RY, libevdev.REL_RZ),
		device.EnableEventCodes(libevdev.Type.REL, libevdev.REL_HWHEEL, libevdev.REL_DIAL),
		device.EnableEventCodes(libevdev.Type.REL, libevdev.REL_MISC),
		device.EnableEventCodes(libevdev.Type.KEY, libevdev.BTN_LEFT),
		device.EnableEventCodes(libevdev.Type.KEY, libevdev.BTN_MIDDLE),
		device.EnableEventCodes(libevdev.Type.KEY, libevdev.BTN_RIGHT),
	)
	return device, err
}
