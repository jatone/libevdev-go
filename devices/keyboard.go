package devices

import "bitbucket.org/jatone/libevdev"

// DefaultKeyboard enables generic keyboard event codes for a device.
func DefaultKeyboard(device libevdev.Device) (libevdev.Device, error) {
	err := checkForErrors(
		device.EnableEventCodes(libevdev.Type.KEY, numpad...),
		device.EnableEventCodes(libevdev.Type.KEY, directional...),
		device.EnableEventCodes(libevdev.Type.KEY, keyboard...),
		device.EnableEventCodes(libevdev.Type.KEY, function...),
		device.EnableEventCodes(libevdev.Type.KEY, programmable...),
		device.EnableEventCodes(libevdev.Type.KEY, navigation...),
	)

	return device, err
}

// NewNamedDevice creates a new device with no events enabled.
func NewNamedDevice(name string) (device libevdev.Device) {
	device = libevdev.NewDevice()
	device.SetName(name)
	return
}

// checks for errors and returns first error.
func checkForErrors(errors ...error) error {
	for _, err := range errors {
		if err != nil {
			return err
		}
	}
	return nil
}

// KeySets Convience grouping of different types of keys.
var KeySets = keysets{
	Directional:  directional,
	Numpad:       numpad,
	function:     function,
	SoundControl: soundControl,
	MediaControl: mediaControl,
	programmable: programmable,
	Navigation:   navigation,
	Power:        power,
	Keyboard:     keyboard,
}

type keysets struct {
	Directional,
	Numpad,
	function,
	SoundControl,
	MediaControl,
	programmable,
	Navigation,
	Power,
	Keyboard []int
}

var directional = []int{
	libevdev.KEY_UP,
	libevdev.KEY_DOWN,
	libevdev.KEY_LEFT,
	libevdev.KEY_RIGHT,
}

var numpad = []int{
	libevdev.KEY_KP1,
	libevdev.KEY_KP2,
	libevdev.KEY_KP3,
	libevdev.KEY_KP4,
	libevdev.KEY_KP5,
	libevdev.KEY_KP6,
	libevdev.KEY_KP7,
	libevdev.KEY_KP8,
	libevdev.KEY_KP9,
	libevdev.KEY_KP0,
	libevdev.KEY_KPDOT,
	libevdev.KEY_KPPLUS,
	libevdev.KEY_KPMINUS,
	libevdev.KEY_KPENTER,
	libevdev.KEY_KPSLASH,
	libevdev.KEY_KPASTERISK,
	libevdev.KEY_NUMLOCK,
	libevdev.KEY_KPEQUAL,
	libevdev.KEY_KPPLUSMINUS,
	libevdev.KEY_KPJPCOMMA,
	libevdev.KEY_KPCOMMA,
	libevdev.KEY_KPLEFTPAREN,
	libevdev.KEY_KPRIGHTPAREN,
}

var function = []int{
	libevdev.KEY_F1,
	libevdev.KEY_F2,
	libevdev.KEY_F3,
	libevdev.KEY_F4,
	libevdev.KEY_F5,
	libevdev.KEY_F6,
	libevdev.KEY_F7,
	libevdev.KEY_F8,
	libevdev.KEY_F9,
	libevdev.KEY_F10,
	libevdev.KEY_F11,
	libevdev.KEY_F12,
	libevdev.KEY_F13,
	libevdev.KEY_F14,
	libevdev.KEY_F15,
	libevdev.KEY_F16,
	libevdev.KEY_F17,
	libevdev.KEY_F18,
	libevdev.KEY_F19,
	libevdev.KEY_F20,
	libevdev.KEY_F21,
	libevdev.KEY_F22,
	libevdev.KEY_F23,
	libevdev.KEY_F24,
}

var soundControl = []int{
	libevdev.KEY_MUTE,
	libevdev.KEY_VOLUMEDOWN,
	libevdev.KEY_VOLUMEUP,
	libevdev.KEY_BASSBOOST,
}

var mediaControl = []int{
	libevdev.KEY_PLAYPAUSE,
	libevdev.KEY_PAUSE,
	libevdev.KEY_PREVIOUSSONG,
	libevdev.KEY_NEXTSONG,
	libevdev.KEY_STOP,
	libevdev.KEY_AGAIN,
	libevdev.KEY_REWIND,
	libevdev.KEY_PLAY,
	libevdev.KEY_BACK,
	libevdev.KEY_FORWARD,
	libevdev.KEY_CLOSE,
	libevdev.KEY_FASTFORWARD,
}

var programmable = []int{
	libevdev.KEY_PROG1,
	libevdev.KEY_PROG2,
	libevdev.KEY_PROG3,
	libevdev.KEY_PROG4,
}

var navigation = []int{
	libevdev.KEY_INSERT,
	libevdev.KEY_HOME,
	libevdev.KEY_PAGEUP,
	libevdev.KEY_DELETE,
	libevdev.KEY_END,
	libevdev.KEY_PAGEDOWN,
}

var power = []int{
	libevdev.KEY_SLEEP,
	libevdev.KEY_WAKEUP,
}

var keyboard = []int{
	libevdev.KEY_ESC,
	libevdev.KEY_1,
	libevdev.KEY_2,
	libevdev.KEY_3,
	libevdev.KEY_4,
	libevdev.KEY_5,
	libevdev.KEY_6,
	libevdev.KEY_7,
	libevdev.KEY_8,
	libevdev.KEY_9,
	libevdev.KEY_0,
	libevdev.KEY_MINUS,
	libevdev.KEY_EQUAL,
	libevdev.KEY_BACKSPACE,
	libevdev.KEY_TAB,
	libevdev.KEY_Q,
	libevdev.KEY_W,
	libevdev.KEY_E,
	libevdev.KEY_R,
	libevdev.KEY_T,
	libevdev.KEY_Y,
	libevdev.KEY_U,
	libevdev.KEY_I,
	libevdev.KEY_O,
	libevdev.KEY_P,
	libevdev.KEY_LEFTBRACE,
	libevdev.KEY_RIGHTBRACE,
	libevdev.KEY_BACKSLASH,
	libevdev.KEY_CAPSLOCK,
	libevdev.KEY_A,
	libevdev.KEY_S,
	libevdev.KEY_D,
	libevdev.KEY_F,
	libevdev.KEY_G,
	libevdev.KEY_H,
	libevdev.KEY_J,
	libevdev.KEY_K,
	libevdev.KEY_L,
	libevdev.KEY_SEMICOLON,
	libevdev.KEY_APOSTROPHE,
	libevdev.KEY_ENTER,
	libevdev.KEY_LEFTSHIFT,
	libevdev.KEY_Z,
	libevdev.KEY_X,
	libevdev.KEY_C,
	libevdev.KEY_V,
	libevdev.KEY_B,
	libevdev.KEY_N,
	libevdev.KEY_M,
	libevdev.KEY_COMMA,
	libevdev.KEY_DOT,
	libevdev.KEY_SLASH,
	libevdev.KEY_RIGHTSHIFT,
	libevdev.KEY_LEFTCTRL,
	libevdev.KEY_LEFTMETA,
	libevdev.KEY_LEFTALT,
	libevdev.KEY_SPACE,
	libevdev.KEY_RIGHTALT,
	libevdev.KEY_RIGHTMETA,
	libevdev.KEY_RIGHTCTRL,
}

// 	KEY_GRAVE
// 	KEY_SCROLLLOCK
// 	KEY_ZENKAKUHANKAKU
// 	KEY_102ND
// 	KEY_RO
// 	KEY_KATAKANA
// 	KEY_HIRAGANA
// 	KEY_HENKAN
// 	KEY_KATAKANAHIRAGANA
// 	KEY_MUHENKAN
// 	KEY_SYSRQ
// 	KEY_LINEFEED
// 	KEY_MACRO
// 	KEY_HANGUEL
// 	KEY_HANJA
// 	KEY_YEN
// 	KEY_COMPOSE
// 	KEY_PROPS
// 	KEY_UNDO
// 	KEY_FRONT
// 	KEY_COPY
// 	KEY_OPEN
// 	KEY_PASTE
// 	KEY_FIND
// 	KEY_CUT
// 	KEY_HELP
// 	KEY_MENU
// 	KEY_CALC
// 	KEY_SETUP
// 	KEY_FILE
// 	KEY_SENDFILE
// 	KEY_DELETEFILE
// 	KEY_XFER
// 	KEY_WWW
// 	KEY_MSDOS
// 	KEY_COFFEE
// 	KEY_DIRECTION
// 	KEY_CYCLEWINDOWS
// 	KEY_MAIL
// 	KEY_BOOKMARKS
// 	KEY_COMPUTER
// 	KEY_PLAYCD
// 	KEY_PAUSECD
// 	KEY_CLOSECD
// 	KEY_EJECTCD
// 	KEY_EJECTCLOSECD
// 	KEY_STOPCD
// 	KEY_RECORD
// 	KEY_PHONE
// 	KEY_ISO
// 	KEY_CONFIG
// 	KEY_HOMEPAGE
// 	KEY_REFRESH
// 	KEY_EXIT
// 	KEY_MOVE
// 	KEY_EDIT
// 	KEY_SCROLLUP
// 	KEY_SCROLLDOWN
// 	KEY_SUSPEND
// 	KEY_PRINT
// 	KEY_HP
// 	KEY_CAMERA
// 	KEY_SOUND
// 	KEY_QUESTION
// 	KEY_EMAIL
// 	KEY_CHAT
// 	KEY_SEARCH
// 	KEY_CONNECT
// 	KEY_FINANCE
// 	KEY_SPORT
// 	KEY_SHOP
// 	KEY_ALTERASE
// 	KEY_CANCEL
// 	KEY_BRIGHTNESSDOWN
// 	KEY_BRIGHTNESSUP
// 	KEY_MEDIA
