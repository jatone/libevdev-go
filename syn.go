package libevdev

/*
#include <linux/input.h>
*/
import "C"

const (
	SYN_REPORT    = int(C.SYN_REPORT)
	SYN_CONFIG    = int(C.SYN_CONFIG)
	SYN_MT_REPORT = int(C.SYN_MT_REPORT)
	SYN_DROPPED   = int(C.SYN_DROPPED)
	SYN_MAX       = int(C.SYN_MAX)
	SYN_CNT       = int(C.SYN_CNT)
)
